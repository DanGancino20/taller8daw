import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { tablaComponent } from 'src/app/tabla/tabla.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Taller8Angular';
}
